package lab2;

public class Entity {
    double health, power, armor;
    boolean isAlive = true;
    Entity(double health, double power, double armor){
        this.health=health;
        this.power = power;
        this.armor=armor;
    }
    Entity(){}
    public void fight(Entity attacker) {
        this.health -= attacker.power/this.armor;
        if (this.health <= 0) {
            this.isAlive = false;
        }
    }
}
