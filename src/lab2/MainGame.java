package lab2;

// Пример реализации наследования
public class MainGame {
    public static void main(String[] args){
        Player Donts = new Player();
        Enemy Zlodey = new Enemy(120, 10, 1.1);
        while(Donts.isAlive || Zlodey.isAlive){
            Donts.fight(Zlodey);
            System.out.println("Donts бьёт Злодея. Здоровье Donts="+Donts.health+" здоровье Злодея="+Zlodey.health);
            if (!Donts.isAlive){
                break;
            }
            Zlodey.fight(Donts);
            System.out.println("Злодей бьёт Donts. Здоровье Donts="+Donts.health+" здоровье Злодея="+Zlodey.health);
        }
        if (Donts.health > Zlodey.health){
            System.out.println("\nDonts победил! :)");
        } else{
            System.out.println("\nЗлодей победил! :(");
        }
    }
}
