package lab4;

// Пример реализации static
import java.util.Date;

public class Main {
    public static void main(String[] args){
        final int N = 10;
        Animal[] animals = new Animal[N];
        for (int i=0; i<N; i++) {
            animals[i] = new Animal();
            animals[i].Roar();
        }
        animals[1].Roars(); //вызов static метода у проинициализированного объекта
        Animal.Roars(); //вызов static метода напрямую через класс
    }
}
