package lab3;

public class Chicken extends Animal {
    boolean abilityToFly;
    Chicken(float size, String name, boolean ability){
            super(size, name);
            this.abilityToFly = ability;
            if (abilityToFly){
                System.out.println("Created animal(chicken) is able to fly\n");
            } else{
                System.out.println("Created animal(chicken) is unable to fly\n");
            }

        }

    public void bark(){
        System.out.println("Chicken says 'ko-ko-ko'");
    }
    public void fly(){
        if (abilityToFly){
            System.out.println("Look! This chicken flying! :)");
        } else{
            System.out.println("Oh no. This chicken can't fly! :(");
        }
    }
}
