package lab3;

//Пример реализации конструкторов классов
public class Main {
    public static void main(String[] args){
        Wolf volk = new Wolf(1.87f, "Eric", 7.7f);
        volk.bark();
        volk.go();
        System.out.println("_______________________________\n");
        Chicken ryaba = new Chicken(0.3f, "Ryaba", true);
        ryaba.bark();
        ryaba.fly();
    }
}