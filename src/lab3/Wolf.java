package lab3;

public class Wolf extends Animal {
    float speed;
    Wolf(float size, String name, float speed){
        super(size, name);
        this.speed = speed;
        System.out.println("Speed of created Animal(Wold) = " + speed + "km/h\n");
    }
    public void bark(){
        System.out.println("Wolf says 'woof! woof!'");
    }
    public void go(){
        System.out.println("Running with " + speed + "km/h");
    }
}
