package lab1;

// Пример работы со строками
public class Strings {
    public static void main(String[] args){
        String str1 = "Freaking brothers every way like M.J.";
        String str2 = "I can't believe, today was a good day";
        System.out.println("Длинна строки: ("+str2+") = "+ str2.length()+" символов");

        System.out.println(str1);
        System.out.println("\n__Производим конкатенацию двух строк__\n");
        String str3 = str1.concat("\n" + str2);
        System.out.println(str3);

        System.out.println("\n__Производим сравнение двух строк__\n");
        String str4 = "I got to say it was a good day";
        if (str2.equals(str4)){
            System.out.println("Строка ("+ str2 + ") и ("+str4+") равны");
        } else {
            System.out.println("Строка ("+ str2 + ") и ("+str4+") не равны");
        }

        System.out.println("\n__Производим замену в строке__\n");
        String str5 = str1;
        System.out.println("Строка до изменения: "+str5);
        str5 = str1.replace("M.J.", "A.K.");
        System.out.println("Строка после изменения: "+str5);
        
    }
}