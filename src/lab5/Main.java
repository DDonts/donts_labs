package lab5;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.FileNotFoundException;

public class Main {
    public static void main(String[] args) throws IOException {
        try {
            BufferedReader reader = new BufferedReader(new FileReader("C:\\Users\\Username\\Desktop\\test.txt"));
            String firstString = reader.readLine();
            System.out.println(firstString);
        } catch (FileNotFoundException e) {
            System.out.println("Ошибка! Файл не найден!");
        } catch (IOException e) {
            System.out.println("Ошибка при вводе/выводе данных из файла!");
            e.printStackTrace();
        }   finally {
            System.out.println("Конец обработки первого исключения!");
        }
        
        try {
            System.out.println(366/0);
        } catch (ArithmeticException e) {
            System.out.println("Ошибка! Деление на 0!");
        } finally {
                System.out.println("Конец обработки второго исключения!");
        }
    }
}
